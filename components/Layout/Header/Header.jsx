import Link from 'next/link';
import Head from 'next/head';
import { useRouter } from 'next/router';
import React from "react";

import favicon from "../../../public/images/favicon.ico";
import logo from '../../../public/images/logo.svg';
import bag from '../../../public/images/bag.svg';
import profile from '../../../public/images/profile.svg';

import './Header.scss';

const Header = () => {

    const browserStorage = (typeof localStorage === 'undefined') ? [] : localStorage;

    const router = useRouter();

    return (
        <header className="header">
            <Head>
                <meta charSet="utf-8"/>
                <link rel="shortcut icon" type="image/x-icon" href={favicon} />
                <meta name="viewport" content="width=1300, user-scalable=yes" />
                <title>Chocolate</title>
            </Head>
            <div className="content_block header_block">
                <Link href="/">
                    <a>
                        <img src={logo} alt="logo"/>
                    </a>
                </Link>
                <div className="user_menu">
                    <Link href="/">
                        <a>
                            Shop
                        </a>
                    </Link>
                    <Link href="/">
                        <a>
                            Build your own box
                        </a>
                    </Link>
                    <Link href="/">
                        <a>
                            Refer a friend
                        </a>
                    </Link>
                    <Link href="/community">
                        <a>
                            Community
                        </a>
                    </Link>
                    <Link href="/">
                        <a>
                            #VirginChoc
                        </a>
                    </Link>
                    <Link href="/story">
                        <a>
                            About
                        </a>
                    </Link>
                </div>
                <div className="user_info">
                    <Link href="/">
                        <a>
                            <img src={bag} alt="bag"/>
                        </a>
                    </Link>
                    <Link href="/">
                        <a>
                            <img src={profile} alt="profile"/>
                        </a>
                    </Link>
                </div>
            </div>
        </header>
    )
};

export default Header;