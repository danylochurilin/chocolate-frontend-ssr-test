import Link from 'next/link';
import React, { useState } from 'react';
import { useRouter } from "next/router";
import FormControl from '@material-ui/core/FormControl';

import SelectComponent from '../../HelperComponents/SelectComponent/SelectComponent';
import { getOption } from '../../HelperComponents/functions';

import logo from '../../../public/images/logo.svg';
import instagram from '../../../public/images/instagram.svg';
import facebook from '../../../public/images/facebook.svg';
import youtube from '../../../public/images/youtube.svg';
import twitter from '../../../public/images/twitter.svg';
import pinterest from '../../../public/images/pinterest.svg';

import './Footer.scss';

const Footer = () => {
    const [language, setLanguage] = useState({ label: getOption('ENG'), value: "1" });
    const [languages, setLanguages] = useState([
        {label: getOption('ENG'), value: "1" },
        {label: getOption('RU'), value: "2" }
    ]);

    const router = useRouter();

    const handleChange = name => event => {
        setLanguage({ [name]: event });
    };

    return (
        <footer className="footer">
            <div className="content_block">
                <div className="footer_wrapper">
                    <div className="">
                        <Link href="/">
                            <a>
                                <img src={logo} alt="logo"/>
                            </a>
                        </Link>
                    </div>
                    <div>
                        <div className="title">Shop</div>
                        <Link href="/">
                            <a>
                                Shop All
                            </a>
                        </Link>
                        <Link href="/">
                            <a>
                                Wholesale
                            </a>
                        </Link>
                    </div>
                    <div>
                        <div className="title">Account</div>
                        <Link href="/">
                            <a>
                                My Account
                            </a>
                        </Link>
                        <Link href="/">
                            <a>
                                Check Order
                            </a>
                        </Link>
                    </div>
                    <div>
                        <div className="title">Support</div>
                        <Link href="/">
                            <a>
                                Contact Us
                            </a>
                        </Link>
                        <Link href="/">
                            <a>
                                FAQ
                            </a>
                        </Link>
                        <Link href="/">
                            <a>
                                Affiliates
                            </a>
                        </Link>
                    </div>
                    <div>
                        <div className="title">About Us</div>
                        <Link href="/">
                            <a>
                                Careers
                            </a>
                        </Link>
                        <Link href="/">
                            <a>
                                Media Center
                            </a>
                        </Link>
                        <Link href="/">
                            <a>
                                Nutrition Collective
                            </a>
                        </Link>
                    </div>
                    <div>
                        <div className="title">Our Mission</div>
                        <Link href="/">
                            <a>
                                StayYoung Foundation
                            </a>
                        </Link>
                        <Link href="/">
                            <a>
                                Donations
                            </a>
                        </Link>
                    </div>
                </div>
                <div className="footer_social">
                    <div className="social">
                        <Link href="/">
                            <a>
                                <img src={facebook} alt="facebook"/>
                            </a>
                        </Link>
                        <Link href="/">
                            <a>
                                <img src={instagram} alt="instagram"/>
                            </a>
                        </Link>
                        <Link href="/">
                            <a>
                                <img src={youtube} alt="youtube"/>
                            </a>
                        </Link>
                        <Link href="/">
                            <a>
                                <img src={twitter} alt="twitter"/>
                            </a>
                        </Link>
                        <Link href="/">
                            <a>
                                <img src={pinterest} alt="pinterest"/>
                            </a>
                        </Link>
                    </div>
                    <div className="block_info_footer">
                        <div className="link_block">
                            <Link href="/">
                                <a>
                                    Privacy
                                </a>
                            </Link>
                            <Link href="/">
                                <a>
                                    Terms & Conditions
                                </a>
                            </Link>
                            <Link href="/">
                                <a>
                                    Terms of Sale
                                </a>
                            </Link>
                            <Link href="/">
                                <a>
                                    EU Supply Chains Act
                                </a>
                            </Link>
                            <Link href="/">
                                <a>
                                    About Our Ads
                                </a>
                            </Link>
                        </div>
                        <FormControl className="select_wrapper">
                            <SelectComponent
                                value={language}
                                options={languages}
                                change={handleChange('language',)}
                                isClearable="false"
                                isSearchable = {false}
                            />
                        </FormControl>
                    </div>
                </div>
            </div>
        </footer>
    )
};

export default Footer;