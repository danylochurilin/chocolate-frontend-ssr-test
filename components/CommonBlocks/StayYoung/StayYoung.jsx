import React, { useState, useEffect, useContext } from 'react';

import { ButtonLoader } from '../../HelperComponents/Loaders/Loaders';
import { emailRegExp } from '../../HelperComponents/regExp';

import { AppContext } from '../../../context/appState';

import subscribe_bg from "../../../public/images/subscribe_bg.png";

import './StayYoung.scss';

const StayYoung = () => {
    const [success, setSuccess] = useState(false);
    const [loading, setLoading] = useState(false);
    const [email, setEmail] = useState('');

    const { postSubscribe, subscription, dispatch } = useContext(AppContext);

    const browserStorage = (typeof localStorage === 'undefined') ? [] : localStorage;

    useEffect(() => {
        const email = browserStorage.getItem('email');
        if (email) {
            setSuccess(true);
            setEmail(email);
        }
    }, []);

    useEffect(() => {
        if (subscription === 'error' || subscription === 'success') {
            browserStorage.setItem('email', email);
            setLoading(false);
            setSuccess(true);
        }
    }, [subscription]);

    const handleInput = e => {
        setEmail(e.target.value);
        setSuccess(false);
    };

    const handleSubscription = () => {
        setLoading(true);
        const data = {
            "email": email
        };
        postSubscribe(data, dispatch);
    };

    return (
        <div className="stay_young">
            <div className="content_block">
                <div className="title">Give StayYoung a Try</div>
                <div className="descriptions">Sign up for email to receive 10% off your first order</div>
                <div className="search">
                    <input
                        type="text"
                        placeholder="Your email"
                        value={email}
                        disabled={loading}
                        onChange={handleInput}
                    />
                    {
                        success ?
                            <div className="success_btn"/>
                            :
                            <button
                                className="btn_color"
                                onClick={handleSubscription}
                                disabled={!emailRegExp.test(email)}>
                                {loading ? <ButtonLoader /> : "Subscribe"}
                            </button>
                    }
                </div>
            </div>
            <img src={subscribe_bg} alt="subscribe_bg" className="subscribe_bg"/>
        </div>
    )
};

export default StayYoung;