import React from 'react';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';

import './SpecialDeal.scss';

const SpecialDeal = ({ onClose, className }) => (
      <div className={`special_deal_block ${className}`}>
          <div className="content_block">
              <div>
                  Flash sale! Take 15% off all variety packs.
                  <span>Shop now</span>
              </div>
              <div>
                  <IconButton
                      key="close"
                      aria-label="Close"
                      color="primary"
                      size="small"
                      onClick={onClose}
                  >
                      <CloseIcon />
                  </IconButton>
              </div>
          </div>
      </div>
);

export default SpecialDeal;