import React from "react";

import the_story_face1 from "../../../../public/images/the_story_face1.png";

import "./TheInspiration.scss";

const TheInspiration = () => {
    return (
        <div className="the_inspiration">
            <div className="content_block">
                <div className="title">The inspiration</div>
                <div>
                    <span>
                        It all started in a small health food store in the Old Town of Tallinn, where I got to
                        experiment with superfood ingredients. The surrounding meat restaurants and fast food joints fed
                        my passion and dedication for raw, healthy, vibrant, nutritionally dense foods.
                    </span>
                    <span>
                        I had followed a non-violent diet for decades, but it was only after discovering the raw diet
                        that I fell in love with superfoods. During my first year of following a raw diet, I achieved my
                        personal record of 700 pull-ups in a day. I was 33 years old. I knew I was onto something.
                    </span>
                    <span>
                        The late nights of experimenting at the Old Town health food store resulted in four raw
                        chocolate bars made from only superfood ingredients and coconut sugar: Classic, Goji, Mulberry
                        and Chlorella. These are our bestsellers to date.
                    </span>
                </div>
            </div>
            <img src={the_story_face1} alt="the_story_face1" className="the_inspirationy_img" />
        </div>
    );
};

export default TheInspiration;
