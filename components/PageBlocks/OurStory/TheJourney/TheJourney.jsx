import React from "react";

import the_story_face2 from "../../../../public/images/the_story_face2.png";

import "./TheJourney.scss";

const TheJourney = () => {
    return (
        <div className="the_journey">
            <div className="content_block">
                <div className="title">
                    The <br /> journey
                </div>
                <div>
                    <span>
                        My enthusiasm for raw chocolate moved my friends. With their feedback, I kept developing my
                        recipes, at my chocolate lab in the Old Town store. Working several jobs to finance my product
                        development, I was slowly moving towards turning my dream for StayYoung chocolates into a
                        reality.
                    </span>
                    <span>
                        The breakthrough came when I found a distributor in Moscow. This meant growing beyond the Old
                        Town store and increased volumes that validated that YourStory’s promise was clicking with
                        people. Encouraged by this, I thought, “Why not go a step further? What if I can create
                        chocolates that actually make people healthier?”
                    </span>
                    <span>
                        The key was removing coconut sugar from the recipes. The best superfoods deserve the best
                        sweeteners, which can be Yacon or even banana. And I couldn’t have been more thrilled to see
                        that our customers agreed — the sugar-free raw chocolate bars sold out like there was no
                        tomorrow.
                    </span>
                    <span>
                        Our customers’ confidence in the StayYoung Chocolates keeps inspiring me. I am proud to keep
                        working on chocolates that will make you healthier.
                    </span>
                </div>
            </div>
            <img src={the_story_face2} alt="the_story_face2" className="the_story_face2" />
        </div>
    );
};

export default TheJourney;
