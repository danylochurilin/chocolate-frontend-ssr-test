import React from "react";
import Link from "next/link";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

import Rating from "../../../HelperComponents/Rating/Rating";

import photo from "../../../../public/images/photo.png";
import arrow from "../../../../public/images/arrow_btn.png";
import signature from "../../../../public/images/signature.png";
import the_story_face3 from "../../../../public/images/the_story_face3.png";

import "./YourStory.scss";

const YourStory = () => {
    const settings = {
        dots: false,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1
    };

    return (
        <div className="your_story">
            <div className="content_block">
                <div className="your_story_wrapper">
                    <div className="title">
                        What will <br /> your story be?
                    </div>
                    <div className="descriptions">Try StayYoung and see meet the super you, on superfoods.</div>
                    <div>
                        <img src={signature} alt="signature" className="signature" />
                    </div>
                    <div className="descriptions_info">
                        {" "}
                        <span>Thank you for your appreciation! </span>
                        <br />
                        Horre Saluste, StayYoung Founder, Creative Director and CEO
                    </div>

                    <div className="reviews_all">
                        <div className="reviews_all_">
                            <p>17 reviews</p>
                            <Rating value={2} />
                        </div>
                        <div>
                            <Link href="/">
                                <a>
                                    View All
                                    <img src={arrow} alt="arrow" />
                                </a>
                            </Link>
                        </div>
                    </div>
                    <div>
                        <Slider {...settings}>
                            <div className="slider_items">
                                <div className="icon_box">
                                    <img src={photo} alt="slide_1" />
                                </div>
                                <div className="slider_content">
                                    <div className="rating_comments">
                                        <p>Sean Martinez</p>
                                        <Rating value={2} />
                                    </div>
                                    <div className="descriptions">
                                        Curabitur lobortis id lorem id bibendum. Ut id consectetur magna. Quisque
                                        volutpat augue enim, pulvinar lobortis nibh lacinia at.
                                    </div>
                                </div>
                            </div>
                            <div className="slider_items">
                                <div className="icon_box">
                                    <img src={photo} alt="slide_2" />
                                </div>
                                <div className="slider_content">
                                    <div className="rating_comments">
                                        <p>Kathryn Banks</p>
                                        <Rating value={3} />
                                    </div>
                                    <div className="descriptions">
                                        Curabitur lobortis id lorem id bibendum. Ut id consectetur magna. Quisque
                                        volutpat augue enim, pulvinar lobortis nibh lacinia at.
                                    </div>
                                </div>
                            </div>
                            <div className="slider_items">
                                <div className="icon_box">
                                    <img src={photo} alt="slide_3" />
                                </div>
                                <div className="slider_content">
                                    <div className="rating_comments">
                                        <p>Corey Gregory</p>
                                        <Rating value={4} />
                                    </div>
                                    <div className="descriptions">
                                        Curabitur lobortis id lorem id bibendum. Ut id consectetur magna. Quisque
                                        volutpat augue enim, pulvinar lobortis nibh lacinia at.
                                    </div>
                                </div>
                            </div>
                            <div className="slider_items">
                                <div className="icon_box">
                                    <img src={photo} alt="slide_4" />
                                </div>
                                <div className="slider_content">
                                    <div className="rating_comments">
                                        <p>Corey Gregory</p>
                                        <Rating value={5} />
                                    </div>
                                    <div className="descriptions">
                                        Curabitur lobortis id lorem id bibendum. Ut id consectetur magna. Quisque
                                        volutpat augue enim, pulvinar lobortis nibh lacinia at.
                                    </div>
                                </div>
                            </div>
                        </Slider>
                    </div>
                </div>
            </div>
            <img src={the_story_face3} alt="the_story_face3" className="the_story_face3" />
        </div>
    );
};

export default YourStory;
