import React from "react";

import the_story_img from "../../../../public/images/the_story_img.png";

import "./TheStory.scss";

const TheStory = () => {
    return (
        <div className="the_story">
            <div className="content_block">
                <div className="title">
                    The Story of <br />
                    StayYoung Chocolate
                </div>
            </div>
            <img src={the_story_img} alt="the_story_img" className="the_story_img" />
        </div>
    );
};

export default TheStory;
