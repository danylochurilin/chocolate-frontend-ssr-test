import React, { Component } from "react";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";

import community_bg9 from "../../../../public/images/communitybg9.png";
import arrow from "../../../../public/images/arrow_down.svg";

import "./Questions.scss";

const Questions = () => {
    return (
        <div className="questions">
            <div className="content_block">
                <div className="title">Frequently asked questions</div>
                <div className="questions_wrapper">
                    <ExpansionPanel className="expansion_panel">
                        <ExpansionPanelSummary aria-controls="panel1a-content" id="panel1a-header" className="head">
                            <div className="expansion_title">
                                <span>Can I cancel my subscription at any time?</span>
                                <div className="arrow">
                                    <img src={arrow} alt="arrow" />
                                </div>
                            </div>
                        </ExpansionPanelSummary>
                        <ExpansionPanelDetails>
                            <div className="expansion_info">
                                Yes, you can cancel whenever you wish or even just skip an order or two to take a break.
                            </div>
                        </ExpansionPanelDetails>
                    </ExpansionPanel>
                    <ExpansionPanel className="expansion_panel">
                        <ExpansionPanelSummary aria-controls="panel1a-content" id="panel1a-header" className="head">
                            <div className="expansion_title">
                                <span>Is there a minimum number of snacks I have to subscribe to?</span>
                                <div className="arrow">
                                    <img src={arrow} alt="arrow" />
                                </div>
                            </div>
                        </ExpansionPanelSummary>
                        <ExpansionPanelDetails>
                            <div className="expansion_info">
                                Yes, you can cancel whenever you wish or even just skip an order or two to take a break.
                            </div>
                        </ExpansionPanelDetails>
                    </ExpansionPanel>
                    <ExpansionPanel className="expansion_panel">
                        <ExpansionPanelSummary aria-controls="panel1a-content" id="panel1a-header" className="head">
                            <div className="expansion_title">
                                <span>Is there free shipping?</span>
                                <div className="arrow">
                                    <img src={arrow} alt="arrow" />
                                </div>
                            </div>
                        </ExpansionPanelSummary>
                        <ExpansionPanelDetails>
                            <div className="expansion_info">
                                Yes, you can cancel whenever you wish or even just skip an order or two to take a break.
                            </div>
                        </ExpansionPanelDetails>
                    </ExpansionPanel>
                    <ExpansionPanel className="expansion_panel">
                        <ExpansionPanelSummary aria-controls="panel1a-content" id="panel1a-header" className="head">
                            <div className="expansion_title">
                                <span>Can I join the StayYoung Community to buy snacks for my office?</span>
                                {/*<div className="arrow"><img src={arrow} alt="arrow"/></div>*/}
                            </div>
                        </ExpansionPanelSummary>
                        <ExpansionPanelDetails>
                            <div className="expansion_info">
                                Yes, you can cancel whenever you wish or even just skip an order or two to take a break.
                            </div>
                        </ExpansionPanelDetails>
                    </ExpansionPanel>
                </div>
            </div>
            <img src={community_bg9} alt="community_bg9" className="communitybg9" />
        </div>
    );
};

export default Questions;
