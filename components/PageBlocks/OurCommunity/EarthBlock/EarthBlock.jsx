import React from "react";

import Community_bg4 from "../../../../public/images/Communitybg4.png";

import "./EarthBlock.scss";

const EarthBlock = () => {
    return (
        <div className="earth_wrapper">
            <div className="content_block">
                <div className="left">
                    <div className="title">Kind to our Earth</div>
                    <span>
                        Every year, we plant trees as our give-back plan. In 2018, we planted 500 trees and we plan to
                        keep this up as a tradition every year. Furthermore, our team is involved with environmental
                        restoration projects and always source our ingredients ethically.
                    </span>
                </div>
                <div className="right">
                    <div className="title">Good for your insides and the Earth</div>
                    <span>
                        Your subscriptions support our efforts toward environmental balance. Beyond planting trees and
                        environmental restoration projects, we also support ethical farming and advocate for clean
                        eating to keep our environment clean.
                    </span>
                </div>
            </div>
            <img src={Community_bg4} alt="Community_bg4" className="Communitybg4" />
        </div>
    );
};

export default EarthBlock;
