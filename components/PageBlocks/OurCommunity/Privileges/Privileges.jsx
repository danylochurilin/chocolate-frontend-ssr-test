import React from "react";

import privileges from "../../../../public/images/privileges.png";

import "./Privileges.scss";

const Privileges = () => {
    return (
        <div className="privileges_wrapper">
            <div className="content_block">
                <div className="title">
                    Privileges of the <br /> StayYoung Community
                </div>
                <div className="privileges_info">
                    <div>
                        <span>- Subscribe once, receive every month</span>
                        <p>Sign up just once, and you’ll receive StayYoung chocolate bars every month.</p>
                    </div>
                    <div>
                        <span>- Good for the Earth</span>
                        <p>With your support, we can plant trees each year as our give-back plan.</p>
                    </div>
                    <div>
                        <span>- Free shipping</span>
                        <p>All StayYoung Community subscriptions of $20 or more get free shipping.</p>
                    </div>
                    <div>
                        <span>- New and limited edition products</span>
                        <p>
                            You'll receive limited edition bars, and you'll get early access to any new products we
                            make.
                        </p>
                    </div>
                    <div>
                        <span>- Free samples</span>
                        <p>
                            We'll include one of our favorite chocolates with superfoods – on us! – with every <br />{" "}
                            recurring order.
                        </p>
                    </div>
                    <div>
                        <span>- No-worry order management</span>
                        <p>
                            We'll send you order reminders before shipping and you can always let us know when <br />{" "}
                            you want to skip an order.
                        </p>
                    </div>
                </div>
            </div>
            <img src={privileges} alt="privileges" className="privileges" />
        </div>
    );
};

export default Privileges;
