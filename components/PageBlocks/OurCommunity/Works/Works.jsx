import React from "react";

import conditions_bg3 from "../../../../public/images/conditionsbg3.png";

import "./Works.scss";

const Works = () => {
    return (
        <div className="works_wrapper">
            <div className="content_block">
                <div className="title">
                    How it <br />
                    works
                </div>
                <div>
                    <div>
                        <span>
                            Skip the effort of going to the middle man at the store and subscribe to the StayYoung
                            Chocolate Subscription.
                        </span>
                        <p>1</p>
                    </div>
                    <div>
                        <span>
                            We craft the chocolate bars for your subscription with care in Tallinn, Estonia using
                            ethically traded cacao, coconut milk and coconut sugar. Note that all of the chocolate bars
                            are free of dairy, soy, gluten free, agave, GMO and do not contain refined sugar.
                        </span>
                        <p>2</p>
                    </div>
                    <div>
                        <span>
                            Every month, you receive an assortment of StayYoung chocolate bars, including limited
                            edition flavors made exclusively for our subscribers and select bars from our lineup of
                            award-winning classics.
                        </span>
                        <p>3</p>
                    </div>
                </div>
            </div>
            <img src={conditions_bg3} alt="conditions_bg3" className="conditionsbg3" />
        </div>
    );
};

export default Works;
