import React from "react";

import arrow from "../../../../public/images/arrow_btn.png";
import own_box from "../../../../public/images/own_box.png";
import subscribe from "../../../../public/images/subscribe.png";
import Shape_brown_large from "../../../../public/images/Shape_brown_large.png";

import "./BlockCardFavorite.scss";

const BlockCardFavorite = () => {
    return (
        <div className="card_favorite ">
            <div className="content_block">
                <div className="block_shop block_shop_1">
                    <div className="block_info">
                        <div>
                            subscribe to your <br /> favorite chocolate
                        </div>
                        <span>
                            Already have a tried-and-true favorite? Choose how often <br /> you'd like to receive it and
                            we'll take care of the rest.
                        </span>
                        <button>
                            Shop Now <img src={arrow} alt="arrow" />
                        </button>
                    </div>
                    <div className="block_shop_img">
                        <img src={subscribe} alt="subscribe" />
                    </div>
                </div>
                <div className="block_shop block_shop_2">
                    <div className="block_info">
                        <div>
                            Get started by <br /> Your Own Box
                        </div>
                        <span>
                            Build your own custom variety set and fill it with your favorites <br /> from our full
                            assortment of tasty and healthy chocolate with <br /> superfoods.
                        </span>
                        <button>
                            Build now
                            <img src={arrow} alt="arrow" />
                        </button>
                    </div>
                    <div className="block_shop_img">
                        <img src={own_box} alt="own_box" />
                    </div>
                </div>
            </div>

            <img src={Shape_brown_large} alt="Shape_brown_large" className="shape_brown_large" />
        </div>
    );
};

export default BlockCardFavorite;
