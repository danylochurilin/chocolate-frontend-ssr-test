import React, { useState, useEffect } from "react";
import Link from "next/link";

import Card from "../../../HelperComponents/Card/Card";

import image_1 from "../../../../public/images/image_1.png";
import image_2 from "../../../../public/images/image_2.png";
import image_3 from "../../../../public/images/image_3.png";
import image_4 from "../../../../public/images/image_4.png";
import arrow from "../../../../public/images/Path 6.svg";
import Shape_grey_large from "../../../../public/images/Shape_grey_large.png";

import "./OurCommunityFavorites.scss";

const OurCommunityFavorites = () => {
    const [innerHeight, setHeight] = useState(null);
    const [innerWidth, setWidth] = useState(null);

    useEffect(() => {
        setHeight(document.getElementById("card_block").getBoundingClientRect().height);
        setWidth(document.getElementById("card_block").getBoundingClientRect().width);
    }, []);

    return (
        <div className="our_community_favorites">
            <div className="content_block">
                <div className="title">Try our community favorites</div>
                <div className="community_favorites_wrapper">
                    <div id="card_block">
                        <Card innerHeight={innerHeight} innerWidth={innerWidth} idx={0} image={image_1}>
                            <div className="text">
                                Chaga <br />
                                Set
                            </div>
                        </Card>
                        <Link href="/">
                            <a>
                                Quick shop <img src={arrow} alt="arrow" />
                            </a>
                        </Link>
                    </div>
                    <div>
                        <Card innerHeight={innerHeight} innerWidth={innerWidth} idx={1} image={image_2}>
                            <div className="text">
                                Blueberry <br />
                                Set
                            </div>
                        </Card>
                        <Link href="/">
                            <a>
                                Quick shop <img src={arrow} alt="arrow" />
                            </a>
                        </Link>
                    </div>
                    <div>
                        <Card innerHeight={innerHeight} innerWidth={innerWidth} idx={2} image={image_3}>
                            <div className="text">
                                Mulberry & <br /> Vanilla Set
                            </div>
                        </Card>
                        <Link href="/">
                            <a>
                                Quick shop <img src={arrow} alt="arrow" />
                            </a>
                        </Link>
                    </div>
                    <div>
                        <Card innerHeight={innerHeight} innerWidth={innerWidth} idx={3} image={image_4}>
                            <div className="text">
                                Sea Buckthorn
                                <br /> Set
                            </div>
                        </Card>
                        <Link href="/">
                            <a>
                                Quick shop <img src={arrow} alt="arrow" />
                            </a>
                        </Link>
                    </div>
                </div>
            </div>
            <img src={Shape_grey_large} alt="Shape_grey_large" className="shape_grey_large" />
        </div>
    );
};

export default OurCommunityFavorites;
