import React, { Component } from "react";
import Link from "next/link";

import community_bg1 from "../../../../public/images/communitybg1.png";
import time_icon from "../../../../public/images/time_icon.svg";
import arrow from "../../../../public/images/arrow_btn.png";

import "./Community.scss";

const Community = () => {
    return (
        <div className="community_wrapper">
            <div className="content_block">
                <div className="title">Our Community</div>
                <div className="descriptions">
                    Join our community by subscribing to our StayYoung Community Subscription. With <br /> this
                    subscription, you’ll get an assortment of chocolates delivered to your door every <br /> month and
                    do good to the Earth.
                </div>
                <div className="members">
                    <span>Existing members,</span>
                    <Link href="/">
                        <a className="text_link">
                            Log in here
                            <img src={arrow} alt="arrow" />
                        </a>
                    </Link>
                </div>
                <button className="btn_color">Get €8</button>
            </div>
            <div className="position_block">
                <div>
                    <img src={time_icon} alt="time_icon" className="time_icon" />
                </div>
                <span>
                    Flexible deliveries & <br /> easy order reminders
                </span>
            </div>
            <img src={community_bg1} alt="community_bg1" className="communitybg1" />
        </div>
    );
};

export default Community;
