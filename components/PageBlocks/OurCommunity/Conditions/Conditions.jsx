import React from "react";

import community_bg2 from "../../../../public/images/communitybg2.png";
import coconut from "../../../../public/images/coconut.png";
import ethical from "../../../../public/images/ethical.png";
import organic from "../../../../public/images/organic.png";
import dairy from "../../../../public/images/dairy.png";
import gmo from "../../../../public/images/gmo.png";
import soy from "../../../../public/images/soy.png";
import compostable from "../../../../public/images/compostable.png";

import "./Conditions.scss";

const Conditions = () => {
    return (
        <div className="conditions_wrapper">
            <div className="content_block">
                <div>
                    <div>
                        <img src={organic} alt="organic" className="organic" />
                    </div>
                    <span>
                        Made with organic <br /> & wildcrafted <br /> ingredients
                    </span>
                </div>
                <div>
                    <div>
                        <img src={ethical} alt="ethical" className="ethical" />
                    </div>
                    <span>
                        Ethically <br />
                        traded
                    </span>
                </div>
                <div>
                    <div>
                        <img src={coconut} alt="coconut" className="coconut" />
                    </div>
                    <span>
                        {" "}
                        More than 50% of <br />
                        the chocolates are <br />
                        sugarfree. Less than <br />
                        50% of the bars are <br />
                        made with coconut <br />
                        sugar.
                    </span>
                </div>
                <div>
                    <div>
                        <img src={compostable} alt="compostable" className="compostable" />
                    </div>
                    <span>
                        100% compostable <br /> packaging
                    </span>
                </div>
                <div>
                    <div>
                        <img src={dairy} alt="dairy" className="dairy" />
                    </div>
                    <span>Dairy free</span>
                </div>
                <div>
                    <div>
                        <img src={soy} alt="soy" className="soy" />
                    </div>
                    <span>Soy free</span>
                </div>
                <div>
                    <div>
                        <img src={gmo} alt="gmo" className="gmo" />
                    </div>
                    <span>GMO free</span>
                </div>
            </div>
            <img src={community_bg2} alt="community_bg2" className="communitybg2" />
        </div>
    );
};

export default Conditions;
