import React from "react";

import ManageAccountbg from "../../../../public/images/ManageAccountbg.png";

import "./ManageAccount.scss";

const ManageAccount = () => {
    return (
        <div className="manage_account">
            <div className="content_block">
                <div className="title">Manage account</div>
                <button className="btn_color">Log in</button>
            </div>
            <img src={ManageAccountbg} alt="ManageAccountbg" className="ManageAccountbg" />
        </div>
    );
};

export default ManageAccount;
