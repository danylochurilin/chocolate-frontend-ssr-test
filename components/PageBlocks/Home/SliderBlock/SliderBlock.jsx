import React, { useState } from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

import slide_1 from "../../../../public/images/slide_1.png";
import slide_2 from "../../../../public/images/slide_2.png";
import slide_3 from "../../../../public/images/slide_3.png";
import play_bg from "../../../../public/images/play_bg.png";

import "./SliderBlock.scss";

const SliderBlock = () => {
    const [currentIndex, setIndex] = useState(1);

    const slides = [
        { id: 0, image: slide_1, string: "We design <br/> the world’s <br/> most advanced <br/> chocolate" },
        { id: 1, image: slide_2, string: "We make 100% <br/> natural chocolate" },
        { id: 2, image: slide_3, string: "A Chocolate that <br/> loves you back! <br/> How?" }
    ];

    const afterChange = id => {
        setIndex(id + 1);
    };

    const settings = {
        dots: false,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1,
        afterChange: afterChange
    };

    return (
        <div className="slider_block">
            <Slider {...settings}>
                {slides.map(el => (
                    <div className="slider" key={el.id}>
                        <div className="content_block">
                            <div className="title" dangerouslySetInnerHTML={{ __html: el.string }} />
                            <button className="btn_more btn_color">LEARN MORE</button>
                        </div>
                        <img src={el.image} alt="slider_1" className="icon_slider" />
                    </div>
                ))}
            </Slider>
            <div className="slider_info">
                <div className="text">
                    <span className="active">{currentIndex}</span>
                    <p>/ {slides.length}</p>
                </div>
                <div className="line-slider" style={{ width: `${(currentIndex / slides.length) * 100}%` }} />
            </div>
            <div className="block_video">
                <div className="video">
                    <img src={play_bg} alt="play_bg" />
                </div>
                <div className="text">
                    We are <br />
                    StayYoung Chocolate
                </div>
            </div>
        </div>
    );
};

export default SliderBlock;
