import React, { Component } from "react";
import Link from "next/link";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

import Rating from "../../../HelperComponents/Rating/Rating";

import Shape_brown_small from "../../../../public/images/Shape_brown_small.png";
import photo from "../../../../public/images/photo.png";
import arrow from "../../../../public/images/arrow_btn.png";

import "./Reviews.scss";

const Reviews = () => {
    const settings = {
        dots: false,
        infinite: true,
        speed: 300,
        slidesToShow: 3,
        slidesToScroll: 1
    };

    return (
        <div className="reviews">
            <div className="content_block">
                <div className="info_reviews">
                    <div className="title">
                        Real Reviews <br />
                        from Real Customers
                    </div>
                    <div className="reviews_all">
                        <div className="reviews_all_">
                            <p>17 reviews</p>
                            <Rating value={2} />
                        </div>
                        <div>
                            <Link href="/">
                                <a>
                                    View All
                                    <img src={arrow} alt="arrow" />
                                </a>
                            </Link>
                        </div>
                    </div>
                </div>
                <Slider {...settings}>
                    <div className="slider_items">
                        <div className="icon_box">
                            <img src={photo} alt="slide_1" />
                        </div>
                        <div className="slider_content">
                            <div className="rating_comments">
                                <p>Sean Martinez</p>
                                <Rating value={2} />
                            </div>
                            <div className="descriptions">
                                Curabitur lobortis id lorem id bibendum. Ut id consectetur magna. Quisque volutpat augue
                                enim, pulvinar lobortis nibh lacinia at.
                            </div>
                        </div>
                    </div>
                    <div className="slider_items">
                        <div className="icon_box">
                            <img src={photo} alt="slide_2" />
                        </div>
                        <div className="slider_content">
                            <div className="rating_comments">
                                <p>Kathryn Banks</p>
                                <Rating value={3} />
                            </div>
                            <div className="descriptions">
                                Curabitur lobortis id lorem id bibendum. Ut id consectetur magna. Quisque volutpat augue
                                enim, pulvinar lobortis nibh lacinia at.
                            </div>
                        </div>
                    </div>
                    <div className="slider_items">
                        <div className="icon_box">
                            <img src={photo} alt="slide_3" />
                        </div>
                        <div className="slider_content">
                            <div className="rating_comments">
                                <p>Corey Gregory</p>
                                <Rating value={4} />
                            </div>
                            <div className="descriptions">
                                Curabitur lobortis id lorem id bibendum. Ut id consectetur magna. Quisque volutpat augue
                                enim, pulvinar lobortis nibh lacinia at.
                            </div>
                        </div>
                    </div>
                    <div className="slider_items">
                        <div className="icon_box">
                            <img src={photo} alt="slide_4" />
                        </div>
                        <div className="slider_content">
                            <div className="rating_comments">
                                <p>Corey Gregory</p>
                                <Rating value={5} />
                            </div>
                            <div className="descriptions">
                                Curabitur lobortis id lorem id bibendum. Ut id consectetur magna. Quisque volutpat augue
                                enim, pulvinar lobortis nibh lacinia at.
                            </div>
                        </div>
                    </div>
                </Slider>
            </div>
            <img src={Shape_brown_small} alt="Shape_brown_small" className="shape_brown_small" />
        </div>
    );
};

export default Reviews;
