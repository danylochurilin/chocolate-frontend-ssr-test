import React from "react";
import Link from "next/link";

import Shape_brown_large2 from "../../../../public/images/Shape_brown_large2.png";
import arrow from "../../../../public/images/arrow_btn.png";
import blog_post1 from "../../../../public/images/image_blog1.png";
import blog_post2 from "../../../../public/images/image_blog2.png";
import blog_post3 from "../../../../public/images/image_blog3.png";
import blog_post4 from "../../../../public/images/image_blog4.png";

import "./BlogPosts.scss";

const BlogPosts = () => {
    return (
        <div className="blog_posts">
            <div className="content_block">
                <div className="title">Blog Posts</div>
                <div className="blog_posts_wrapper">
                    <div>
                        <Link href="/">
                            <a className="image_link">
                                <div className="box_icon">
                                    <img src={blog_post1} alt="blog_post" />
                                </div>
                            </a>
                        </Link>
                        <Link href="/">
                            <a className="header_link">
                                <div className="text_title">Donec facilisis tortor ut augue</div>
                            </a>
                        </Link>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pretium pretium tempor. Ut eget
                            imperdiet neque. In volutpat ante semper diam molestie, et aliquam erat laoreet. Sed sit
                            amet arcu aliquet, molestie justo at, auct.
                        </p>
                        <Link href="/">
                            <a className="text_link">
                                Read More
                                <img src={arrow} alt="arrow" />
                            </a>
                        </Link>
                    </div>
                    <div>
                        <Link href="/">
                            <a className="image_link">
                                <div className="box_icon">
                                    <img src={blog_post2} alt="blog_post" />
                                </div>
                            </a>
                        </Link>
                        <Link href="/">
                            <a className="header_link">
                                <div className="text_title">Donec facilisis tortor ut augue</div>
                            </a>
                        </Link>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pretium pretium tempor. Ut eget
                            imperdiet neque. In volutpat ante semper diam molestie, et aliquam erat laoreet. Sed sit
                            amet arcu aliquet, molestie justo at, auct.
                        </p>
                        <Link href="/">
                            <a className="text_link">
                                Read More
                                <img src={arrow} alt="arrow" />
                            </a>
                        </Link>
                    </div>
                    <div>
                        <Link href="/">
                            <a className="image_link">
                                <div className="box_icon">
                                    <img src={blog_post3} alt="blog_post" />
                                </div>
                            </a>
                        </Link>
                        <Link href="/">
                            <a className="header_link">
                                <div className="text_title">Donec facilisis tortor ut augue</div>
                            </a>
                        </Link>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pretium pretium tempor. Ut eget
                            imperdiet neque. In volutpat ante semper diam molestie, et aliquam erat laoreet. Sed sit
                            amet arcu aliquet, molestie justo at, auct.
                        </p>
                        <Link href="/">
                            <a className="text_link">
                                Read More
                                <img src={arrow} alt="arrow" />
                            </a>
                        </Link>
                    </div>
                    <div>
                        <Link href="/">
                            <a className="image_link">
                                <div className="box_icon">
                                    <img src={blog_post4} alt="blog_post" />
                                </div>
                            </a>
                        </Link>
                        <Link href="/">
                            <a className="header_link">
                                <div className="text_title">Donec facilisis tortor ut augue</div>
                            </a>
                        </Link>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pretium pretium tempor. Ut eget
                            imperdiet neque. In volutpat ante semper diam molestie, et aliquam erat laoreet. Sed sit
                            amet arcu aliquet, molestie justo at, auct.
                        </p>
                        <Link href="/">
                            <a className="text_link">
                                Read More
                                <img src={arrow} alt="arrow" />
                            </a>
                        </Link>
                    </div>
                </div>
            </div>
            <img src={Shape_brown_large2} alt="Shape_brown_large2" className="Shape_brown_large2" />
        </div>
    );
};

export default BlogPosts;
