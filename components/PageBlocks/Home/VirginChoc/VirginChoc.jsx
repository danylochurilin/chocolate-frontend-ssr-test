import React from "react";

import Shape_grey_small from "../../../../public/images/Shape_grey_small.png";
import icon1 from "../../../../public/images/image_insta1.png";
import icon2 from "../../../../public/images/image_insta2.png";
import icon3 from "../../../../public/images/image_insta3.png";
import icon4 from "../../../../public/images/image_insta4.png";
import icon5 from "../../../../public/images/image_insta5.png";
import likes_icon from "../../../../public/images/likes_icon.svg";
import comments_icon from "../../../../public/images/comments_icon.svg";
import instagram_logo from "../../../../public/images/instagram-logo-white.svg";

import "./VirginChoc.scss";

const VirginChoc = () => {
    return (
        <div className="virgin_choc">
            <div className="content_block">
                <div className="title">#VirginChoc on Instagram</div>
                <div className="wrapper_block">
                    <div>
                        <div className="box_icon">
                            <img src={icon1} alt="icon1" />
                        </div>
                        <div className="text_wrapper">
                            <div>
                                <img src={instagram_logo} alt="instagram_logo" />
                            </div>
                            <div className="box_info">
                                <div>
                                    <img src={likes_icon} alt="likes_icon" />
                                    292
                                </div>
                                <div>
                                    <img src={comments_icon} alt="comments_icon" />
                                    51
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div className="box_icon">
                            <img src={icon2} alt="icon2" />
                        </div>
                        <div className="text_wrapper">
                            <div>
                                <img src={instagram_logo} alt="instagram_logo" />
                            </div>
                            <div className="box_info">
                                <div>
                                    <img src={likes_icon} alt="likes_icon" />
                                    292
                                </div>
                                <div>
                                    <img src={comments_icon} alt="comments_icon" />
                                    51
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div className="box_icon">
                            <img src={icon3} alt="icon3" />
                        </div>
                        <div className="text_wrapper">
                            <div>
                                <img src={instagram_logo} alt="instagram_logo" />
                            </div>
                            <div className="box_info">
                                <div>
                                    <img src={likes_icon} alt="likes_icon" />
                                    292
                                </div>
                                <div>
                                    <img src={comments_icon} alt="comments_icon" />
                                    51
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div className="box_icon">
                            <img src={icon4} alt="icon4" />
                        </div>
                        <div className="text_wrapper">
                            <div>
                                <img src={instagram_logo} alt="instagram_logo" />
                            </div>
                            <div className="box_info">
                                <div>
                                    <img src={likes_icon} alt="likes_icon" />
                                    292
                                </div>
                                <div>
                                    <img src={comments_icon} alt="comments_icon" />
                                    51
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div className="box_icon">
                            <img src={icon5} alt="icon5" />
                        </div>
                        <div className="text_wrapper">
                            <div>
                                <img src={instagram_logo} alt="instagram_logo" />
                            </div>
                            <div className="box_info">
                                <div>
                                    <img src={likes_icon} alt="likes_icon" />
                                    292
                                </div>
                                <div>
                                    <img src={comments_icon} alt="comments_icon" />
                                    51
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <img src={Shape_grey_small} alt="Shape_grey_small" className="shape_grey_small" />
        </div>
    );
};

export default VirginChoc;
