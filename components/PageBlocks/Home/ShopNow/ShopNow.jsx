import React from "react";

import icon1 from "../../../../public/images/Group 5.png";
import icon2 from "../../../../public/images/Group 5 (1).png";
import arrow from "../../../../public/images/arrow_btn.png";
import Shape_brown_large from "../../../../public/images/Shape_brown_large.png";

import "./ShopNow.scss";

const ShopNow = () => {
    return (
        <div className="shop_now ">
            <div className="content_block">
                <div className="block_shop block_shop_1">
                    <div className="block_info">
                        <div>
                            Shop <br />
                            Bars
                        </div>
                        <button>
                            Shop Now <img src={arrow} alt="arrow" />
                        </button>
                    </div>
                    <div className="block_shop_img">
                        <img src={icon2} alt="icon2" />
                    </div>
                </div>
                <div className="block_shop block_shop_2">
                    <div className="block_info">
                        <div>
                            Build Your <br /> Own Box
                        </div>
                        <button>
                            Learn More <img src={arrow} alt="arrow" />
                        </button>
                    </div>
                    <div className="block_shop_img">
                        <img src={icon1} alt="icon1" />
                    </div>
                </div>
            </div>
            <img src={Shape_brown_large} alt="Shape_brown_large" className="shape_brown_large" />
        </div>
    );
};

export default ShopNow;
