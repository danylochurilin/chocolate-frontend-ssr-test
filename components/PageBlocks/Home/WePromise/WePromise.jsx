import React from "react";
import Link from "next/link";

import promise_bg from "../../../../public/images/promise_bg_new.png";
import arrow from "../../../../public/images/Path 6.svg";
// import Shape_orange_opacity from "../../../assets/image/Shape_orange_opacity.png";

import "./WePromise.scss";

const WePromise = () => {
    return (
        <div className="we_promise">
            <div className="content_block">
                <div className="title">We promise</div>
                <div className="descriptions">
                    Real food, <br />
                    wholesome <br /> ingredients
                </div>
                <p>
                    StayYoung Chocolate has always been <br /> committed to bringing you wholesome and <br /> delicious
                    snacks. The first and predominant <br /> ingredients in all of our snacks will always be <br /> a
                    nutrient-dense food like nuts, whole grains <br /> or fruits.
                </p>
                <div className="link_block">
                    <Link href="/">
                        <a>
                            Learn More <img src={arrow} alt="arrow" />
                        </a>
                    </Link>
                </div>
            </div>
            <img src={promise_bg} alt="promise_bg" className="promise_bg" />
            {/*<img src={Shape_orange_opacity} alt="Shape_brown_small" className="shape_orange_opacity"/>*/}
        </div>
    );
};

export default WePromise;
