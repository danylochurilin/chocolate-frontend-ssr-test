import fetch from 'isomorphic-unfetch';

export const sendRequest = (url, method, body = undefined) => {
    let options = {
        method: method,
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json;charset=UTF-8'
        }
    };
    if (body) {
        options.body = JSON.stringify(body);
    }
    if (localStorage.token) {
        options.headers = { ...options.headers, 'Authorization': 'Token ' + localStorage.getItem('token')}
    }
    return fetch(url, options);
};

export function getOption(label) {
    return (
        <div className="status_select">
            <div>{label}</div>
        </div>
    )
}