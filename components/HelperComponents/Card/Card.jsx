import React from 'react'
import { useSpring, animated } from 'react-spring'
import './Card.scss'

const Card = ({ innerHeight, innerWidth, idx, image, children }) => {
    const [props, set] = useSpring(() => ({ xys: [0, 0, 1], config: { mass: 5, tension: 350, friction: 40 } }));
    const calc = (x, y) => [-(y - innerHeight / 2) / 20, (x - innerWidth / 2) / 10, 1.1];
    const trans = (x, y, s) => `perspective(600px) rotateX(${x}deg) rotateY(${y}deg) scale(${s})`;
    if (!innerHeight) return null;
    return (
        <animated.div
            className="card"
            onMouseMove={({ clientX: x, clientY: y }) => set({ xys: calc(x - document.getElementsByClassName('card')[idx].getBoundingClientRect().left, y - document.getElementsByClassName('card')[idx].getBoundingClientRect().top) })}
            onMouseLeave={() => set({ xys: [0, 0, 1] })}
            style={{
                transform: props.xys.interpolate(trans),
                backgroundImage: `url(${image})`
            }}
        >
            {children}
        </animated.div>
    )
};

export default Card;