import React from "react";

import Layout from "../../components/Layout/Container";
import StayYoung from "../../components/CommonBlocks/StayYoung/StayYoung";
import TheStory from "../../components/PageBlocks/OurStory/TheStory/TheStory";
import TheInspiration from "../../components/PageBlocks/OurStory/TheInspiration/TheInspiration";
import TheJourney from "../../components/PageBlocks/OurStory/TheJourney/TheJourney";
import YourStory from "../../components/PageBlocks/OurStory/YourStory/YourStory";

import "../../components/PageBlocks/OurStory/OurStory.scss";

const OurCommunity = () => {
    return (
        <Layout>
            <div className="our_story">
                <TheStory />
                <TheInspiration />
                <TheJourney />
                <YourStory />
                <StayYoung />
            </div>
        </Layout>
    );
};

export default OurCommunity;
