import React from "react";
import Layout from "../components/Layout/Container";
import SliderBlock from "../components/PageBlocks/Home/SliderBlock/SliderBlock";
import ShopNow from "../components/PageBlocks/Home/ShopNow/ShopNow";
import CommunityFavorites from "../components/PageBlocks/Home/CommunityFavorites/CommunityFavorites";
import Reviews from "../components/PageBlocks/Home/Reviews/Reviews";
import WePromise from "../components/PageBlocks/Home/WePromise/WePromise";
import VirginChoc from "../components/PageBlocks/Home/VirginChoc/VirginChoc";
import BlogPosts from "../components/PageBlocks/Home/BlogPosts/BlogPosts";
import StayYoung from "../components/CommonBlocks/StayYoung/StayYoung";

import "../components/PageBlocks/Home/Home.scss";

const Home = () => (
    <Layout>
        <div className="dashboard">
            <SliderBlock />
            <ShopNow />
            <CommunityFavorites />
            <Reviews />
            <WePromise />
            <VirginChoc />
            <BlogPosts />
            <StayYoung />
        </div>
    </Layout>
);

export default Home;
