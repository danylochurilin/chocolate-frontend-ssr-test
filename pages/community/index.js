import React from "react";

import Layout from "../../components/Layout/Container";
import StayYoung from "../../components/CommonBlocks/StayYoung/StayYoung";
import Community from "../../components/PageBlocks/OurCommunity/Community/Community";
import Conditions from "../../components/PageBlocks/OurCommunity/Conditions/Conditions";
import Privileges from "../../components/PageBlocks/OurCommunity/Privileges/Privileges";
import ManageAccount from "../../components/PageBlocks/OurCommunity/ManageAccount/ManageAccount";
import EarthBlock from "../../components/PageBlocks/OurCommunity/EarthBlock/EarthBlock";
import Questions from "../../components/PageBlocks/OurCommunity/Questions/Questions";
import OurCommunityFavorites from "../../components/PageBlocks/OurCommunity/OurCommunityFavorites/OurCommunityFavorites";
import BlockCardFavorite from "../../components/PageBlocks/OurCommunity/BlockCardFavorite/BlockCardFavorite";
import Works from "../../components/PageBlocks/OurCommunity/Works/Works";

import "../../components/PageBlocks/OurCommunity/OurCommunity.scss";

const OurCommunity = () => {
    return (
        <Layout>
            <div className="our_community">
                <Community />
                <Conditions />
                <Works />
                <EarthBlock />
                <BlockCardFavorite />
                <Privileges />
                <OurCommunityFavorites />
                <ManageAccount />
                <Questions />
                <StayYoung />
            </div>
        </Layout>
    );
};

export default OurCommunity;
