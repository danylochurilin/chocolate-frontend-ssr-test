export const initialAuthState = {
    auth_error: ''
};

export const AuthReducer = (state, action) => {
    switch(action.type) {
        case 'LOGIN':
            return {
                ...state,
                auth_error: action.payload
            };

        default:
            return state;
    }
};